import React, {Component} from 'react';
import UserCreate from "./UserCreate";
import {LanguageStore} from "../contexts/LanguageContext";
import ColorContext from '../contexts/ColorContext'
import LanguageSelector from "./languageSelector";

class App extends Component {
    // We don't need the static contextType if we're using provider :)
    // static contextType = LanguageContext;
    render() {
        console.log(this.context)
        return (
            <LanguageStore>
                <div className={'ui container'}>
                    <LanguageSelector/>
                    <ColorContext.Provider value={'red'}>
                        <UserCreate/>
                    </ColorContext.Provider>
                </div>
            </LanguageStore>
        );
    }
}

export default App;