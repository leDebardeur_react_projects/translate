import React, {Component} from 'react';
import LanguageContext from "../contexts/LanguageContext";

class Field extends Component {
    // contextType is only required if we're using this.context to get data out of context
    static contextType = LanguageContext;
    render() {
        const text = this.context.language==='english'? 'Name' : 'Naam';
        return (
            <div className={'ui field'}>
                <label>{text}</label>
                <input/>
            </div>
        );
    }
}

export default Field;